export default class Sound {
    id: number;
    symbol: string;
	name: string;
	audio_file: string;
	description: string;
	primary_category: string;
	secondary_category: string;
	tertiary_category: string;
	voiced: number;
	rounded: number;
	url: string;

    constructor(id = 1, symbol = '', name = '', audio_file = '', description = '', primary_category, secondary_category, tertiary_category, voiced, rounded, url) {
        this.id = id;
        this.symbol = symbol;
		this.name = name;
		this.audio_file = audio_file;
		this.description = description;
		this.primary_category = primary_category;
		this.secondary_category = secondary_category;
		this.tertiary_category = tertiary_category;
		this.voiced = voiced;
		this.rounded = rounded;
		this.url = url;
    }

    clone() {
        return new Sound(this.id, this.symbol, this.name, this.audio_file, this.description, this.primary_category, this.secondary_category, this.tertiary_category, this.voiced, this.rounded, this.url);
    }
}