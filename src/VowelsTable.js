import React from 'react';
import { ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
//import DatabaseManager from './DatabaseManager';

interface IProps {
}

interface iState {
    vowelList: [];
    note: string;
}

export default class VowelsTable extends React.Component<IProps, iState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            vowelList: [],
            note: ''
        }
    }
	
	componentDidMount(): void {
		console.log("In componentDidMount");
		this.updateList();
	}
	
	updateList(): void {
		//DatabaseManager.SelectQuery();
		/*DatabaseManager.getVowels()
			.then((result) => {
				this.setState({
					vowelList: result
				})
			});*/
	}
	
	render() {
		return (
			<ScrollView>
				<DataTable>
					<DataTable.Header>
					  <DataTable.Title>Symbol</DataTable.Title>
					  <DataTable.Title>Primary</DataTable.Title>
					  <DataTable.Title>Age</DataTable.Title>
					</DataTable.Header>
					{
						this.state.vowelList.map((item, index) => {
							return (
								<DataTable.Row>
								  <DataTable.Cell>{ item.symbol }</DataTable.Cell>
								  <DataTable.Cell>{ item.secondary_category }</DataTable.Cell>
								  <DataTable.Cell>{ item.tertiary_category  }</DataTable.Cell>
								</DataTable.Row>
							);
						})
					}
					
				</DataTable>
			</ScrollView>
		);
	}
	
}