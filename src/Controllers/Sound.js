// see https://github.com/uit2712/SQLiteSimpleCRUDExample/blob/master/controllers/HeroController.js

import Sound from '../models/Sound';
import Message from '../models/Message';

let SQLite = require('react-native-sqlite-storage');
let sqlite = SQLite.openDatabase({ name: 'sound-db'});


// result: realm objects
export const getAllSounds = () => {
    return new Promise((resolve, reject) => {
        let msg = new Message();
        msg.result = [];
        sqlite.transaction((tx) => {
            tx.executeSql('SELECT * FROM sound', [], (tx, results) => {
                for (let i = 0; i < results.rows.length; i++) {
                    let item = results.rows.item(i);
                    let sound = new Sound(item.SoundId, item.SoundName);
                    msg.result.push(sound);
                }
                msg.message = 'Get all sounds successfully!';
                resolve({ result: msg.result, message: msg.message });
            }, (error) => {
                msg.result = [];
                msg.message = `${error.message}`;
                resolve({ result: msg.result, message: msg.message });
            });
        })
    });
}

// result: realm object
export const getSoundById = (id: number) => {
    return new Promise((resolve, reject) => {
        let msg = new Message();
        sqlite.transaction((tx) => {
            tx.executeSql('SELECT * FROM sound WHERE id = ?', [id], (tx, results) => {
                if (results.rows.length > 0) {
                    let item = results.rows.item(0);
                    let sound = new Sound(item.id, item.name);
                    msg.result = sound;
                    msg.message = `Found 1 sound with id = ${id}`;
                } else {
                    msg.result = null;
                    msg.message = `Not found sound with id = ${id}`;
                }
                resolve({ result: msg.result, message: msg.message });
            }, (error) => {
                msg.result = null;
                msg.message = `${error.message}`;
                resolve({ result: msg.result, message: msg.message });
            });
        })
    });
}

const checkIfSoundExists = (id: number) => {
    getSoundById(id).then(({ result, message }) => {
        let msg = new Message();
        msg.result = sound != null;
        if (msg.result)
            msg.message = `Found 1 sound with id = ${id}`;
        else msg.message = `Not found sound with id = ${id}`;
        resolve({ result: msg.result, message: msg.message });
    });
}

export const updateSound = (sound: Sound) => {
    return new Promise((resolve, reject) => {
        let msg = new Message();
        if (!sound) {
            msg.result = false;
            msg.message = 'Invalid sound input!';
            resolve({ result: msg.result, message: msg.message });
        }

        sqlite.transaction((tx) => {
            tx.executeSql('UPDATE Sound SET name = ? WHERE id = ?', [sound.name, sound.id], (tx, results) => {
                if (results.rowsAffected > 0) {
                    msg.result = true;
                    msg.message = 'Update sound successfully!';
                } else {
                    msg.result = false;
                    msg.message = 'Update sound failed!';
                }
                resolve({ result: msg.result, message: msg.message });
            }, (error) => {
                msg.result = false;
                msg.message = `${error.message}`;
                resolve({ result: msg.result, message: msg.message });
            });
        })
    });
}

export const deleteSound = (sound: Sound) => {
    return new Promise((resolve, reject) => {
        let msg = new Message();
        if (!sound) {
            msg.result = false;
            msg.message = 'Invalid sound input!';
            resolve({ result: msg.result, message: msg.message });
        }

        sqlite.transaction((tx) => {
            tx.executeSql('DELETE FROM Sound WHERE SoundId=?', [sound.soundId], (tx, results) => {
                if (results.rowsAffected > 0) {
                    msg.result = true;
                    msg.message = `Delete sound with id=${sound.soundId} successfully!`;
                } else {
                    msg.result = false;
                    msg.message = `Not found sound with id=${sound.soundId}`;
                }
                resolve({ result: msg.result, message: msg.message });
            }, (error) => {
                msg.result = false;
                msg.message = `${error.message}`;
                resolve({ result: msg.result, message: msg.message });
            });
        })
    });
}