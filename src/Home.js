import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import ListFetch from './ListFetch';

class Home extends Component {
	state = {
		count: 0
	}

	onPress = () => {
		this.setState({
		  count: this.state.count + 1
		})
	}

	render() {
		return (
			<View >
				<TouchableOpacity onPress={this.onPress} >
					<Text>Click me</Text>
				</TouchableOpacity>
				<View>
					<Text>
						You clicked { this.state.count } times
					</Text>
				</View>
				<ListFetch/>
			</View>
		);
	}
}

export default Home;