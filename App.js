import * as React from 'react';
import { AppRegistry } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import Home from './src/Home.js';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
};


export default function App() 
{
	return (
	<PaperProvider theme={theme}>
		<Home/>
	</PaperProvider>
	);
}

AppRegistry.registerComponent("Sound", () => App);